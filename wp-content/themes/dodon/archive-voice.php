<?php get_header(); ?>

<div id="content" role="main">

<!-- パンくずリスト -->
<div id="bredcrumb" >
<a href="<?php echo get_option('home'); ?>">Home</a> &raquo;
<?php
$queried_object = get_queried_object();
echo $queried_object -> labels -> name;
?>
</div>


<?php get_template_part( 'pagetitle' ); ?>

<!-- コンテンツ領域開始 -->
<?php
$post_name = $post -> post_name;
?>
<div id="post" class="<?php echo $post_name; ?>">

<div class="entry-content">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<h4><?php the_title(); ?></h4>
	<?php the_content(); ?>

<?php endwhile;endif; ?>
</div>

<?php if ( function_exists( 'wp_pagenavi' ) ) wp_pagenavi(); ?>

</div>
<!-- コンテンツ領域終了 -->

<!-- お問い合わせ共通ブロック -->
<?php
$page = get_page_by_title( '衣装について' );
if ( $post -> post_parent == $page -> ID ) get_template_part( 'content', 'costume_footer' );
?>

<?php get_template_part( 'contact' ); ?>


</div><!-- #content -->


<?php get_sidebar(); ?>
<?php get_footer(); ?>
