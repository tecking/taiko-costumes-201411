<?php
/*
Template Name: ボトムページ
*/
?>
<?php get_header(); ?>

<div id="content" role="main">

<!-- パンくずリスト -->
<div id="bredcrumb" >
<a href="<?php echo get_option('home'); ?>">Home</a> &raquo; 
<?php foreach ( array_reverse(get_post_ancestors($post->ID)) as $parid ) { ?>
<a href="<?php echo get_page_link( $parid );?>" title="<?php echo get_page($parid)->post_title; ?>">
<?php echo get_page($parid)->post_title; ?></a> &raquo; 
<?php } ?>
<?php the_title(); ?>
</div>


<?php get_template_part( 'pagetitle' ); ?>

<!-- コンテンツ領域開始 -->
<?php
$post_name = $post -> post_name;
?>
<div id="post" class="<?php echo $post_name; ?>">
	<div class="entry-content">

		<div class="detail">
			<h3>太鼓専門ボトム</h3>
			<p>「上着はできたけれど、ボトムに何を合わせていいか迷う」という相談が多数寄せられました。そこでオーダー太鼓衣装屋ドドーンでは、お客様にお選びいただけるボトムを各種揃えました。またオリジナルボトムも考案しました。
	婦人アパレルで経験豊富なパタンナーと共に改良を重ねて制作した自慢のパンツです。</p>
			<h3>オーダー太鼓衣装屋ドドーンのボトムの特長</h3>
			<ul>
				<li>ラインがきれいに見えるよう、シルエットにこだわりました。</li>
				<li>深めの股上（またがみ）で腰回りをしっかり包みます。</li>
				<li>脱ぎ着しやすいゴムタイプやマジックテープ仕様。</li>
				<li>裾（すそ）はお客様自身で調整できます（一部除く）。</li>
			</ul>
		</div>

		<div class="detail">
			<h3>すそ絞りパンツ</h3>
			<div class="clearfix">
				<div class="left"><img src="<?php echo get_stylesheet_directory_uri();?>/img/bottom/photo_susoshibori.jpg" alt="すそ絞りパンツ"></div>
				<div>
					<p>ニッカポッカのラインを取り入れて太鼓用に動きやすさを考えたパンツです。</p>
					<p><a class="link" href="<?php echo home_url( 'itemlist/bottom/susoshibori' ); ?>">詳しくはこちら</a></p>
				</div>
			</div>
		</div>

		<div class="detail">
			<h3>すそ絞りパンツ＋脚絆（きゃはん）付き</h3>
			<div class="clearfix">
				<div class="left"><img src="<?php echo get_stylesheet_directory_uri();?>/img/bottom/photo_susoshibori_kyahan.jpg" alt="すそ絞りパンツ＋脚絆（きゃはん）付き"></div>
				<div>
					<p>足さばきが多いチームの方におすすめ。自分好みのシルエットで着こなせる一本です。
</p>
					<p><a class="link" href="<?php echo home_url( 'itemlist/bottom/susoshibori' ); ?>">詳しくはこちら</a></p>
				</div>
			</div>
		</div>

		<div class="detail">
			<h3>フレアーパンツ</h3>
			<div class="clearfix">
				<div class="left"><img src="<?php echo get_stylesheet_directory_uri();?>/img/bottom/photo_flarepants.jpg" alt="フレアーパンツ"></div>
				<div>
					<p>よさこいパンツより幅広です。ボトムが強調され、力強さを演出できます。</p>
					<p><a class="link" href="<?php echo home_url( 'itemlist/bottom/flarepants' ); ?>">詳しくはこちら</a></p>
				</div>
			</div>
		</div>

		<div class="detail">
			<h3>たっつけ袴風（はかまふう）</h3>
			<div class="clearfix">
				<div class="left"><img src="<?php echo get_stylesheet_directory_uri();?>/img/bottom/photo_hakamafupants.jpg" alt="たっつけ袴風（はかまふう）"></div>
				<div>
					<p>ウエストは簡単に着脱できるようにマジックテープまたはヒモを付けました。</p>
					<p><a class="link" href="<?php echo home_url( 'itemlist/bottom/hakamafupants' ); ?>">詳しくはこちら</a></p>
				</div>
			</div>
		</div>

		<div class="detail">
			<h3>袴（はかま）</h3>
			<div class="clearfix">
				<div class="left"><img src="<?php echo get_stylesheet_directory_uri();?>/img/bottom/photo_hakama.jpg" alt="袴（はかま）"></div>
				<div>
					<p>マジックテープタイプ、ヒモで結ぶタイプもあり、着脱が楽ちんです。</p>
					<p><a class="link" href="<?php echo home_url( 'itemlist/bottom/hakama' ); ?>">詳しくはこちら</a></p>
				</div>
			</div>
		</div>

		<div class="detail">
			<h3>よさこいパンツ</h3>
			<div class="clearfix">
				<div class="left"><img src="<?php echo get_stylesheet_directory_uri();?>/img/bottom/photo_yosakoipants.jpg" alt="袴（はかま）"></div>
				<div>
					<p>どんな方にも似合い、はきやすい標準ストレートラインです。</p>
					<p><a class="link" href="<?php echo home_url( 'itemlist/bottom/yosakoipants' ); ?>">詳しくはこちら</a></p>
				</div>
			</div>
		</div>

	</div>
</div>
<!-- コンテンツ領域終了 -->

<!-- お問い合わせ共通ブロック -->
<?php get_template_part( 'contact' ); ?>

</div><!-- #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
