<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

			<div id="content" role="main">

				<!-- 制作事例のカテゴリのみ下記を表示 -->
				<?php if(in_category('showcase')): ?>

					<!-- パンくずリスト -->
					<div id="bredcrumb" >
					<a href="<?php echo get_option('home'); ?>">Home</a> &raquo; 
					
					<?php $cat = get_the_category(); echo get_category_parents($cat[0], true, '  '); ?>					</div>

					<!-- ページタイトル -->
					<div id="pagetitle"><h2><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/contents/pagetitle_work.jpg" alt="制作事例" /></h2></div>

					<div class="entry-content">
						
						<p>太鼓衣装屋ドドーンが手掛けたお客様の衣装制作事例です。</p>
						
						<ul class="worklist">
							<?php
							$posts = get_posts('numberposts=100&category=showcase');
							global $post;
							?>
							<?php
							if($posts): foreach($posts as $post): setup_postdata($post); ?>
							
							<li>
							
							<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
							<img align="left" src="<?php echo catch_that_image(); ?>" alt="<?php the_title(); ?>" />
							</a>
							
							<div class="txtblock">
							<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
							<?php the_excerpt(); ?>
							</div>
							
							</li>
							
							<?php endforeach; endif;
							?>
						</ul>						
						
					</div><!-- .entry-content -->
					

				<!-- 制作事例のカテゴリ以外はこちらを表示 -->
				<?php else: ?>


				<h1 class="page-title"><?php
					printf( __( 'Category Archives: %s', 'twentyten' ), '<span>' . single_cat_title( '', false ) . '</span>' );
				?></h1>
				<?php
					$category_description = category_description();
					if ( ! empty( $category_description ) )
						echo '<div class="archive-meta">' . $category_description . '</div>';

				/* Run the loop for the category page to output the posts.
				 * If you want to overload this in a child theme then include a file
				 * called loop-category.php and that will be used instead.
				 */
				get_template_part( 'loop', 'category' );
				?>

				<!-- 制作事例のカテゴリ以外はこちらを表示 ここまで -->
				<?php endif; ?> 

			</div><!-- #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
