			<div class="contact_area">
				
				<div class="column">
				
				<h3>私達にお聞きください！お客様のご要望を形にいたします。<br />
					お急ぎの方、少数チームの方、遠慮無くお問い合わせを！</h3>
				<p style="font-size:15px;"><span style="background: #eeb3ee">お急ぎの方、小ロット希望の方（初回は5着から、追加は2着から）、オリジナルと既製品を組み合わせたい方</span>、古いハッピを新調したい方、予算に合わせてほしい、などご要望をお聞かせください。できる限りお力になれるよう努力します。<br/>まずはご連絡、ご相談ください。</p>
				
				<div class="btn_contact">
				<a href="<?php echo home_url( '/' ); ?>contactform" ><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/common/bnr_contact.jpg" alt="お見積り・お問い合わせフォーム" /></a>
				</div>
				
				</div>
				
				<h2><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/common/ttl_contact.jpg" alt="お問い合わせはこちらまで・・" /></h2>
				<div id="contact_block">
					<div class="txtblock">
						<h3>オーダー太鼓衣装屋ドドーン<br />（株式会社横山工藝）</h3>
						<p>〒918-8051<br />
						福井県福井市桃園2-1-40<br />
						<img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/common/address.jpg" alt="TEL:0776-36-6720 平日8:30～17:30/土日祝休 FAX:0776-36-6750" class="telbox" />
						</p>
					</div>
					<div class="photoblock"><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/common/photo_staff.jpg" alt="わたし達にお聞きください！" /></div>
				</div>
			
			</div>
