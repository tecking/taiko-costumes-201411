<div id="local_navigation" class="about-costume clearfix">
	<h4>衣装についての関連ページ</h4>
	<ul>
	<?php
	$parent = get_page_by_title( '衣装について' );
	$myposts = get_posts(  array(
		'orderby'			=>	'menu_order',
		'order'				=>	'ASC',
		'post_type'			=>	'page',
		'post_parent'		=>	$parent -> ID,
		'post_status'		=>	'publish' )
	);
	foreach ( $myposts as $post ) : setup_postdata( $post ); next( $myposts ); ?>
	<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><?php if ( current( $myposts ) ) echo '&nbsp;|&nbsp;'; ?></li>
	<?php endforeach; ?>
	</ul>
</div>