<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
	</div><!-- #main -->

	<div id="footer" role="contentinfo">
		<div id="pagetop">
			<p><a href="#top" >ページのトップへ</a></p>
		</div>

<?php
	/* A sidebar in the footer? Yep. You can can customize
	 * your footer with four columns of widgets.
	 */
	get_sidebar( 'footer' );
?>

		<div id="copyright">
			<?php
			$first = '2011'; // Assign the first year.
			$array = getdate();
			$array['year'] == $first ? $str = "&#169;$first" : $str = "&#169;$first-{$array['year']}";
			?>
			<?php echo $str; ?> 太鼓衣装屋ドドーン All Rights Reserved.
		</div><!-- #site-generator -->
		
		<div id="foot_link">
			<a href="http://www.yosakoiya.jp/" target="_blank">よさこい衣装オーダーなら「よさこい屋」</a> | <a href="http://www.o-printya.jp/" target="_blank">オリジナル生地のプリントなら「オーダーぷりんと屋」</a> | <a href="http://www.ykougei.jp/" target="_blank">もっとプリントを知りたいなら「染色工房　横山工藝」</a>
		</div>

	</div><!-- #footer -->

</div><!-- #wrapper -->

<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
</body>
</html>
