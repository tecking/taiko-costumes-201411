<?php

// 個別ページの親ページIDと準ずる子ページを取得

function is_tree($pid) {      // $pid = 指定したページの ID
	global $post;         // $post に現在の固定ページの情報をロード
	 $anc = get_post_ancestors( $post->ID );
	 	foreach($anc as $ancestor) {
	 		if(is_page() && $ancestor == $pid) {
	 		return true;
	 		}
	 	}
	 	if(is_page()&&(is_page($pid)))  
	 		return true;   // 指定したページ、またはそのサブページ
	 	else 
			return false;  // 別のページです
};


// 投稿記事の一番最初の画像srcを取得
function catch_that_image() {
    global $post, $posts;
    $first_img = '';
    ob_start();
    ob_end_clean();
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
    $first_img = $matches [1] [0];
 
if(empty($first_img)){ //Defines a default image
        $first_img = "http://taiko-costumes.jp/wp-content/themes/dodon/img/common/dummy.jpg";
    }
    return $first_img;
}


// カスタムメニューで作成したカスタムリンクに target="_blank" を追記
function my_add_href_atts( $content ) {
	$host = $_SERVER['HTTP_HOST'];
	if ( !preg_match( "/$host/", $content ) ) {
		$content = preg_replace( "/(href=\".+?\")/", "$1 target=\"_blank\"", $content );
	}
	return $content;
}
add_filter( 'walker_nav_menu_start_el', 'my_add_href_atts' );


// フィードURL出力の抑止
add_action( 'after_setup_theme', 'my_setup_child_theme', 11 );
function my_setup_child_theme() {
	if ( !is_admin() ) {
		remove_theme_support( 'automatic-feed-links' );
	}
}

// Authorアーカイブ生成の抑止
add_filter( 'author_rewrite_rules', '__return_empty_array' );


// カスタムメニューの追加
register_nav_menus( array(
	'gallery' => '衣装アルバム',
) );


// 関数ファイルの読み込み
get_template_part( 'functions/js' );
get_template_part( 'functions/itemlist' );
get_template_part( 'functions/thumbnail' );
get_template_part( 'functions/gallery' );
get_template_part( 'functions/pre_get_posts' );
get_template_part( 'functions/custompost' );
get_template_part( 'functions/nextgen' );