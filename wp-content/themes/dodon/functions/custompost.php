<?php

/*
 * カスタム投稿・カスタム分類
 */
add_action( 'init', 'my_create_post_type' );
function my_create_post_type() {
	register_post_type( 'voice', // カスタム投稿タイプの登録
		array(
			'labels' => array(
				'name' => 'お客様の声',
				'singular_name' => 'お客様の声',
				'all_items' => '記事一覧'

			),
			'hierarchical' => false, // true =タクソノミーは親子関係をもつ , false = タクソノミーは親子関係をもたない
			'public' => true,
			'menu_position' => 5,
			'has_archive' => true,
			'supports' => array (
				'title',
				'editor',
				'revisions',
				'author'
			),
			'rewrite' => array(
				'slug' => 'voices'
			)
		)
	);
}
