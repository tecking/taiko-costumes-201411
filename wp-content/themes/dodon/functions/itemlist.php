<?php

/*
 * 衣装の個別ページにスペック欄を追記
 */
//add_filter( 'the_content', 'my_add_spec' );
function my_add_spec( $content ) {
	if ( is_page() && get_fields() ) { // Advance Custom Fields が有効のときのみ機能
		$content .= '<div class="detail"><table><tbody>';
		if ( get_field( 'size' ) ) {
			$obj = get_field_object( 'size' );
			$content .= '<tr><th>' . $obj['label'] . '</th><td>' . get_field( 'size' ) . '</td></tr>';
		}
		if ( get_field( 'material' ) ) {
			$obj = get_field_object( 'material' );
			$content .= '<tr><th>' . $obj['label'] . '</th><td>' . get_field( 'material' ) . '</td></tr>';
		}
		if ( get_field( 'delivery' ) ) {
			$obj = get_field_object( 'delivery' );
			$content .= '<tr><th>' . $obj['label'] . '</th><td>' . get_field( 'delivery' ) . '</td></tr>';
		}
		if ( get_field( 'recommend' ) ) {
			$obj = get_field_object( 'recommend' );
			$content .= '<tr><th>' . $obj['label'] . '</th><td>' . get_field( 'recommend' ) . '</td></tr>';
		}
		if ( get_field( 'price' ) ) {
			$obj = get_field_object( 'price' );
			$content .= '<tr><th>' . $obj['label'] . '</th><td>' . get_field( 'price' ) . '</td></tr>';
		}
		$content .= '</tbody></table></div>';
	}
	return $content;
}
