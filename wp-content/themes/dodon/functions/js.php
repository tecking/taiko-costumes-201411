<?php

/*
 * JavaScriptのロード
 */

add_action( 'wp_enqueue_scripts', 'my_enqueue_scripts' );
function my_enqueue_scripts() {
	if ( !is_admin() ) {
		$dir = get_stylesheet_directory_uri();
		
		// CDNを使う場合はコアのjQueryをderegisterすること
		wp_deregister_script( 'jquery' );
		wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.6.3/jquery.min.js' );

		wp_enqueue_script( 'tile', $dir . '/js/jquery.tile.js' );
		wp_enqueue_script( 'bgSwitcher', $dir . '/js/jquery.bgSwitcher.js' );

		wp_enqueue_script( 'colorbox', $dir . '/js/jquery.colorbox-min.js' );	
		wp_enqueue_script( 'config', $dir . '/js/config.js' );

	}
	return;
}