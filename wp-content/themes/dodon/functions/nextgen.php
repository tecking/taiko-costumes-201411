<?php

/*
 * NextGen Gallery のギャラリーをカスタマイズ
 */
add_filter( 'ngg_show_gallery_content', 'my_ngg_show_gallery_content' );
function my_ngg_show_gallery_content( $out ) {
	//$out = preg_replace(
	return $out;
}


/*
 * NextGen Gallery のギャラリー検索結果をカスタマイズ
 * （要 NextGen 2.0 Search engine プラグイン）
 */
add_filter( 'ngg_gallery_output', 'my_ngg_gallery_output' );
function my_ngg_gallery_output( $out ) {
	if ( function_exists('ngg_images_results') ) {
		return preg_replace( '/(<a.+?title=")(.+?)(".+?<\/a>)/s', "$1$2$3<span>$2</span>", $out );
	}
	return $out;
}


/*
 * NextGen Gallery のギャラリー検索に対応するようカスタムクエリ変数を追加
 * （http://wpdocs.sourceforge.jp/%E9%96%A2%E6%95%B0%E3%83%AA%E3%83%95%E3%82%A1%E3%83%AC%E3%83%B3%E3%82%B9/get_query_var）
 */
add_filter( 'query_vars', 'add_query_vars_filter' );
function add_query_vars_filter( $vars ){
	$vars[] = "nggpage";
	return $vars;
}
