<?php
/*
 * 表示条件を query_posts() や get_posts() でなく pre_get_posts() で制御
 * 
 * この例では、アーカイブページのときに5件の記事を表示する。
 * 別途 query_posts() や get_posts() で指定した表示件数には影響を及ぼさない。
 */


function my_pre_get_posts( $wp_query ) {
	if ( !is_admin() && get_query_var( 'post_type' ) === 'voice' && $wp_query -> is_archive() ) {
		$wp_query -> set( 'posts_per_page', 5 );
	}
}
add_action( 'pre_get_posts', 'my_pre_get_posts' );
