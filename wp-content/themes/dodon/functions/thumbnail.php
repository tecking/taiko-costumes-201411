<?php

// 使用中のテーマでアイキャッチ画像を使えるようにする
add_theme_support( 'post-thumbnails' );

// アイキャッチ画像のサイズを指定する
// 第1引数：横サイズ　第2引数：縦サイズ
// 第3引数：true = 縮小して切り抜く, false = 縮小のみ）
set_post_thumbnail_size( 90, 90 );

// デフォルト以外に作っておきたいアップロード画像のサイズを指定する
// 第1引数：テンプレート内で the_post_thumbnail() で呼び出すときのID
// 第2引数：横サイズ　第3引数：縦サイズ
// 第4引数：true = 縮小して切り抜く, false = 縮小のみ）
//
// 【参考】デフォルトの画像サイズは［設定］－［メディア］にて設定可能

add_image_size( 'itemlist_small_thumbnail', 185, 240, false );
add_image_size( 'itemlist_large_thumbnail', 250, 9999, false );

// 管理画面の「アイキャッチ画像」欄で表示する画像を指定
add_filter( 'admin_post_thumbnail_html', 'my_admin_post_thumbnail_html' );
function my_admin_post_thumbnail_html( $content ) {
	if ( is_admin() ) {
		$patterns = array(
			'/width=".+?" /',
			'/height=".+?" /',
			'/(src=".+?)(-740x198)(\.jpg|\.gif|\.png)/'
		);
		$replacements = array(
			'',
			'',
			'$1-185x240$3'
		);
		return preg_replace( $patterns, $replacements, $content );
	}
}
