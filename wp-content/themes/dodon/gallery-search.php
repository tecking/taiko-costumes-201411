<div id="gallery-search">
	<?php get_search_form(); ?>
	<div class="link">
		<p>人気のキーワード：</p>
		<?php
		$str = wp_nav_menu(array(
			'container' => false,
			'theme_location' => 'gallery',
			'before' => ' | ',
			'echo' => false,
		));
		echo preg_replace( '/(<ul.+?><li.+?>)( \| )/', '$1', $str );
		?>			
	</div>
</div>
