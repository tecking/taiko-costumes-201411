<?php
/*
Template Name: ハッピ（法被）ページ
*/
?>
<?php get_header(); ?>

<div id="content" role="main">

<!-- パンくずリスト -->
<div id="bredcrumb" >
<a href="<?php echo get_option('home'); ?>">Home</a> &raquo; 
<?php foreach ( array_reverse(get_post_ancestors($post->ID)) as $parid ) { ?>
<a href="<?php echo get_page_link( $parid );?>" title="<?php echo get_page($parid)->post_title; ?>">
<?php echo get_page($parid)->post_title; ?></a> &raquo; 
<?php } ?>
<?php the_title(); ?>
</div>


<?php get_template_part( 'pagetitle' ); ?>

<!-- コンテンツ領域開始 -->
<?php
$post_name = $post -> post_name;
?>
<div id="post" class="<?php echo $post_name; ?>">
	<div class="entry-content">

		<div class="detail">
			<h3>本染めハッピ（ほんぞめ）</h3>
			<div class="clearfix">
				<div class="left"><img src="<?php echo get_stylesheet_directory_uri();?>/img/happi/photo_honzome.jpg" alt="本染めハッピ（ほんぞめ）"></div>
				<div>
					<p>本染めは、古典柄や日本伝統色など深い色合いがなじむ染め方です。長持ちもする染め方なので、今後も長く使いたいチームにおすすめしています。</p>
					<p><a class="link" href="<?php echo home_url( 'itemlist/happi/honzome' ); ?>">詳しくはこちら</a></p>
				</div>
			</div>
		</div>

		<div class="detail">
			<h3>フルカラーハッピ</h3>
			<div class="clearfix">
				<div class="left"><img src="<?php echo get_stylesheet_directory_uri();?>/img/happi/photo_fullcolor.jpg" alt="フルカラーハッピ"></div>
				<div>
					<p>前後、衿（えり）にも、自由に図案を入れられます。オリジナル色やデザインにこだわりたい方に好評です。数量が少ない場合はこちらがおすすめです。</p>
					<p><a class="link" href="<?php echo home_url( 'itemlist/happi/fullcolor' ); ?>">詳しくはこちら</a></p>
				</div>
			</div>
		</div>

		<div class="detail">
			<h3>お祭りハッピ</h3>
			<div class="clearfix">
				<div class="left"><img src="<?php echo get_stylesheet_directory_uri();?>/img/happi/photo_matsuri.jpg" alt="本染めハッピ（ほんぞめ）"></div>
				<div>
					<p>町内や地元のお祭りにかかせないお祭りハッピ。学校や地域、公民館の文化祭にも好評です。衿（えり）に名入れ、ロゴ入れができます。</p>
					<p><a class="link" href="<?php echo home_url( 'itemlist/happi/omatsuri' ); ?>">詳しくはこちら</a></p>
				</div>
			</div>
		</div>

		<div class="detail">
			<h3>イベントハッピ（ポリエステル100％　または　綿100％）</h3>
			<div class="clearfix">
				<div class="left"><img src="<?php echo get_stylesheet_directory_uri();?>/img/happi/photo_event.jpg" alt="イベントハッピ"></div>
				<div>
					<p>手軽に、早く、予算に合わせて作りたい。会場でも目立つ存在になりたい！そんな声にお応えして、色や種類を揃えました。</p>
					<p><a class="link" href="<?php echo home_url( 'itemlist/happi/event' ); ?>">詳しくはこちら</a></p>
				</div>
			</div>
		</div>

	</div>
</div>
<!-- コンテンツ領域終了 -->

<!-- お問い合わせ共通ブロック -->
<?php get_template_part( 'contact' ); ?>

</div><!-- #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
