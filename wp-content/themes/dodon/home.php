<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

			<div id="content" role="main">

				<a id="topcatch" href="<?php echo home_url( 'about-costume' ); ?>">
					<ul>
						<li><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/top/top_catch_sub01.jpg" alt="1.あなたの動きにカスタマイズ"></li>
						<li><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/top/top_catch_sub02.jpg" alt="2.1600万色から色選び"></li>
						<li class="end"><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/top/top_catch_sub03.jpg" alt="3.サイズも形も選べます"></li>
					</ul>
				</a>
				<p><a href="<?php echo home_url( 'about-costume' ); ?>">太鼓衣装屋ならではのデザインでご希望にこたえます。</a></p>


				<h2><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/top/ttl_productlist.jpg" alt="衣装一覧"></h2>

				<div id="itemlist" >
				<ul>
					<li><a href="<?php echo home_url( '/' ); ?>itemlist/tachieri" ><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/top/item_tachieri.jpg" alt="立ち衿・ハッピ・半天"></a><p><a href="<?php echo home_url( '/' ); ?>itemlist/tachieri" >人気No.1のオリジナル半天。秘密はファスナーにあり！</a></p></li>
					<li><a href="<?php echo home_url( '/' ); ?>itemlist/happi" ><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/top/item_naire.jpg" alt="ハッピ＋名入れ"></a><p><a href="<?php echo home_url( '/' ); ?>itemlist/happi" >本染め・お祭りハッピ＋名入れ。柄次第で見栄えよい1枚。</a></p></li>
					<li class="end"><a href="<?php echo home_url( '/' ); ?>itemlist/sodenashi" ><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/top/item_sodenashi.jpg" alt="袖なしハッピ"></a><p><a href="<?php echo home_url( '/' ); ?>itemlist/sodenashi" >腕のふりあげがラクなハッピ。はかまと合わせるとグッド♪</a></p></li>
					<li><a href="<?php echo home_url( '/' ); ?>itemlist/bottom" ><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/top/item_bottom.jpg" alt="太鼓専門ボトム"></a><p><a href="<?php echo home_url( '/' ); ?>itemlist/bottom" >ズボン、パンツ、ボトムなど、6種類からえらべます。</a></p></li>
					<li><a href="<?php echo home_url( '/' ); ?>itemlist/inner" ><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/top/item_inner.jpg" alt="インナー・腹掛け"></a><p><a href="<?php echo home_url( '/' ); ?>itemlist/inner" >さらし代わりに使えます。打ちやすくなる1枚です。</a></p></li>
					<li class="end"><a href="<?php echo home_url( '/' ); ?>itemlist/goods" ><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/top/item_goods.jpg" alt="はちまき/手甲/帯/足袋ぐつ"></a><p><a href="<?php echo home_url( '/' ); ?>itemlist/goods" >はちまき／手甲／ワンタッチ帯／たびぐつ</a></p></li>
					<li><a href="<?php echo home_url( '/' ); ?>itemlist/t-shirts" ><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/top/item_t-shirts.jpg" alt="Tシャツ・手ぬぐい"></a><p><a href="<?php echo home_url( '/' ); ?>itemlist/t-shirts" >練習着として好評。オリジナル手ぬぐいも。</a></p></li>
					<li><a href="<?php echo home_url( '/' ); ?>itemlist/makeover" ><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/top/item_makeover.jpg" alt="色あせたハッピを新しく！"></a><p><a href="<?php echo home_url( '/' ); ?>itemlist/makeover" >古くなったハッピを、柄もそのままに新しくします！</a></p></li>
					<!--<li><a href="<?php echo home_url( '/' ); ?>itemlist/order" ><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/top/item_order.jpg" alt="完全オリジナルオーダー"></a><p><a href="<?php echo home_url( '/' ); ?>itemlist/order" >衣装イメージ画をお持ちの方はこちら</a></p></li>-->
					<li class="end"><a href="<?php echo home_url( '/' ); ?>itemlist/logo" ><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/top/item_logo.jpg" alt="ロゴ・柄おつくりします"></a><p><a href="<?php echo home_url( '/' ); ?>itemlist/logo" >衣装作りをきっかけに、ロゴや柄を作りませんか？</a></p></li>
				</ul>
				</div>
				
				<div style="margin-bottom: 20px;">
				<a href="<?php echo get_post_type_archive_link( 'voice' ); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/event_banner_imon.jpg" alt="制作した太鼓チームからの喜びの声" /></a>
				</div>
				
				<div id="blogfeed">
					<dl id="tencho_blog">
						<dt><a href="http://isho-order.com/blog/teams/taiko/" target="_blank"><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/top/blog_tencho.jpg" alt="店長ブログ"></a></dt>
						<dd>
						<?php
						     include_once(ABSPATH . WPINC . '/feed.php');
						     $rss = fetch_feed('http://isho-order.com/blog/atom.xml');  //取得したいRSS
						 
						if (!is_wp_error( $rss ) ) : //エラーがなければ
						     $maxitems = $rss->get_item_quantity(5);  //取得件数
						     $rss_items = $rss->get_items(0, $maxitems);   //指定件数分の配列作成
						endif;
						?>
						 
						<ul>
						<?php
						if ($maxitems == 0)  echo '<li>新しい記事はないようです</li>';
						  else foreach ( $rss_items as $item ) :
						?>
						<li><a href='<?php echo $item->get_permalink(); ?>' target="_blank"><?php echo $item->get_title(); ?></a> (<?php echo $item->get_date("Y-n-j"); ?>)</li>
						<?php endforeach; ?>
						</ul>
						</dd>				
					</dl>

					<dl id="staff_blog">
						<dt><a href="http://isho-order.com/staff/" target="_blank"><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/top/blog_staff.jpg" alt="スタッフ衣装制作ブログ"></a></dt>
						<dd>
						<?php
						     include_once(ABSPATH . WPINC . '/feed.php');
						     $rss = fetch_feed('http://isho-order.com/staff/atom.xml');  //取得したいRSS
						 
						if (!is_wp_error( $rss ) ) : //エラーがなければ
						     $maxitems = $rss->get_item_quantity(5);  //取得件数
						     $rss_items = $rss->get_items(0, $maxitems);   //指定件数分の配列作成
						endif;
						?>
						 
						<ul>
						<?php
						if ($maxitems == 0)  echo '<li>新しい記事はないようです</li>';
						  else foreach ( $rss_items as $item ) :
						?>
						<li><a href='<?php echo $item->get_permalink(); ?>' target="_blank"><?php echo $item->get_title(); ?></a> (<?php echo $item->get_date("Y-n-j"); ?>)</li>
						<?php endforeach; ?>
						</ul>
						</dd>				
					</dl>
					
				</div>

				
				<div id="advice" >
				<a href="<?php echo home_url( '/' ); ?>advice" ><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/top/advice.jpg" alt="衣装制作アドバイス"></a><br>
				<span><a href="<?php echo home_url( '/' ); ?>advice" >太鼓衣装の作り方と注文の流れを、当店スタッフがお伝えします。衣装のことが分からない。何から決めていいか迷っている。そんな方はココをクリック。このページを見ながらお電話ください。</a></span>
				</div>
				
				<div id="gallery" >
				<a href="<?php echo home_url( '/' ); ?>gallery" ><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/top/photoalbum.jpg" alt="衣装アルバム"></a><br>
				<span><a href="<?php echo home_url( '/' ); ?>gallery" >全国各地、海外からも注文を受けています。太鼓チームの希望に合わせて作りました。これからの衣装作りの参考にもなりますよ。</a></span>
				</div>

				<!--
				<div id="customer">
					<h2><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/top/ttl_customer.jpg" alt="お客様の声"></h2>

					<p>○○でした。ありがとうございました。　**県○○さま</p>
					<p>○○でした。ありがとうございました。　**県○○さま</p>
					<p>○○でした。ありがとうございました。　**県○○さま</p>
				
				</div>
				-->
				
				<!-- お問い合わせ共通ブロック -->
				<?php get_template_part( 'contact' ); ?>

			</div><!-- #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
