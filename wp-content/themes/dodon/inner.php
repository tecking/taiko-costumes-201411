<?php
/*
Template Name: インナーページ
*/
?>
<?php get_header(); ?>

<div id="content" role="main">

<!-- パンくずリスト -->
<div id="bredcrumb" >
<a href="<?php echo get_option('home'); ?>">Home</a> &raquo; 
<?php foreach ( array_reverse(get_post_ancestors($post->ID)) as $parid ) { ?>
<a href="<?php echo get_page_link( $parid );?>" title="<?php echo get_page($parid)->post_title; ?>">
<?php echo get_page($parid)->post_title; ?></a> &raquo; 
<?php } ?>
<?php the_title(); ?>
</div>


<?php get_template_part( 'pagetitle' ); ?>

<!-- コンテンツ領域開始 -->
<?php
$post_name = $post -> post_name;
?>
<div id="post" class="<?php echo $post_name; ?>">
	<div class="entry-content">

		<div class="detail">
			<h3>腹掛け＋股引き</h3>
			<div class="clearfix">
 				<div class="left"><img src="<?php echo get_stylesheet_directory_uri();?>/img/inner/haragake.jpg" alt="腹掛け＋股引き"></div>
				<div>
					<p>そのまま着用したり、ハッピのインナーとしても使える腹掛け（どんぶり）。ポケット付きのものや、リバーシブルタイプなど種類も豊富です。</p>
					<a class="link" href="<?php echo home_url( 'itemlist/inner/harakake' ); ?>">詳しくはこちら</a>
				</div>
			</div>
		</div>

		<div class="detail">
			<h3>おしゃれ腹掛け</h3>
			<div class="clearfix">
  				<div class="left"><img src="<?php echo get_stylesheet_directory_uri();?>/img/inner/oshare.jpg" alt="おしゃれ腹掛け"></div>
				<div>
					<p>背中のダブルクロスがかっこいい！お客様の声から生まれた当店のヒット商品です。</p>
					<a class="link" href="<?php echo home_url( 'itemlist/inner/harakake' ); ?>">詳しくはこちら</a>
				</div>
			</div>
		</div>

		<div class="detail">
			<h3>インナー各種（オリジナルノースリーブ、タンクトップなど）</h3>
			<div class="clearfix">
 				<div class="left"><img src="<?php echo get_stylesheet_directory_uri();?>/img/inner/inner.jpg" alt="インナー各種"></div>
				<div>
					<p>半天の下に着ても首元がスッキリ。脇からも下着が見えない細めの袖ぐりに仕上げました。</p>
					<p><a class="link" href="<?php echo home_url( 'itemlist/inner/innerware' ); ?>">詳しくはこちら</a></p>
				</div>
			</div>
		</div>

		<div class="detail">
			<h3>タンクトップなど</h3>
			<div class="clearfix">
 				<div class="left"><img src="<?php echo get_stylesheet_directory_uri();?>/img/inner/original.jpg" alt="タンクトップなど"></div>
				<div>
					<p>腕を振り上げるに演奏にはもってこいの1枚。涼しいと好評です。</p>
					<p><a class="link" href="<?php echo home_url( 'itemlist/inner/innerware' ); ?>">詳しくはこちら</a></p>
				</div>
			</div>
		</div>

	</div>
</div>
<!-- コンテンツ領域終了 -->

<!-- お問い合わせ共通ブロック -->
<?php get_template_part( 'contact' ); ?>

</div><!-- #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
