<?php
/*
Template Name: 商品一覧 詳細ページ
*/
?>
<?php get_header(); ?>

<div id="content" role="main">

<!-- パンくずリスト -->
<div id="bredcrumb" >
<a href="<?php echo get_option('home'); ?>">Home</a> &raquo; 
<?php foreach ( array_reverse(get_post_ancestors($post->ID)) as $parid ) { ?>
<a href="<?php echo get_page_link( $parid );?>" title="<?php echo get_page($parid)->post_title; ?>">
<?php echo get_page($parid)->post_title; ?></a> &raquo; 
<?php } ?>
<?php the_title(); ?>
</div>


<?php //get_template_part( 'pagetitle' ); ?>

<!-- コンテンツ領域開始 -->
<?php
$post_name = $post -> post_name;
?>
<div id="post" class="<?php echo $post_name; ?>">

<div class="entry-content">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div class="prg">
	<h3><?php the_title(); ?></h3>
	<table class="product">
		<tbody>
			<tr class="mainphoto">
				<?php
					$files   = get_post_meta( $post -> ID, '_thumbnail_id', true );
					$file    = wp_get_attachment_url( $files );
					$caption = get_post( $files );

					$blog_link = array(
						'tachieri' => 'costumeitem/tachierihanten',
						'innerware' => 'costumeitem/inner',
						'goods' => 'costumeitem/obi',
						'makeover' => 'costumeitem/happi',
						'honzome' => 'costumeitem/happi',
						'fullcolor' => 'costumeitem/happi',
						'omatsuri' => 'costumeitem/happi',
						'event' => 'costumeitem/happi',
						'sodenashi' => 'costumeitem/sodenashihanten',
						'hakamafupants' => 'costumeitem/tattukehakama',
						'hakama' => 'costumeitem/hakama',
						'harakake' => 'costumeitem/haragakedonburi',
						'susoshibori' => 'costumeitem/pants',
						'flarepants' => 'costumeitem/pants',
						'yosakoipants' => 'costumeitem/pants'
					);
				?>
				<td rowspan="2"><a href="<?php echo clean_url( $file ); ?>" class="colorbox group1"><?php the_post_thumbnail( 'itemlist_large_thumbnail', array( 'alt' => get_the_title(), 'title' => get_the_title() ) ); ?></a><br />
				<?php if ( $caption ) echo $caption -> post_excerpt; ?>
				<td class="description" style="padding-left: 20px;"><?php the_field( 'item_description' ); ?>
					<?php if ( get_field( 'formatted_link', $post -> ID ) ) : ?>
					<br />【<strong>サンプル貸し出しあり</strong>】<br />
					<a class="link" href="<?php echo home_url( 'guide/flow' ); ?>">ご注文の流れについて</a><br />
					<a class="link" href="<?php echo home_url( 'contactform' ); ?>">サンプル無料貸し出しはこちら</a><br />
					<a class="link" href="<?php echo home_url( 'about-costume/material' ); ?>">素材について</a><br />
					綿とポリエステルの違いを説明しています。<br />
					<?php $post_name = $post -> post_name; ?>
						<?php if ( $post_name !== 't-shirts' ) : ?>
						<a class="link" href="http://isho-order.com/blog/<?php echo $blog_link["$post_name"]; ?>" target="_blank">店長ブログはこちら</a><br />
						<?php the_title(); ?>の事例は店長ブログで紹介しています。
						<?php else: ?>
						<p class="link">店長ブログはこちら</p>
						<a href="http://isho-order.com/blog/costumeitem/tshirt/" target="_blank">Tシャツ</a>　<a href="http://isho-order.com/blog/costumeitem/tenugui/" target="_blank">手ぬぐい</a>の事例は店長ブログで紹介しています。
						<?php endif; ?>
					<?php endif; ?>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<?php // ギャラリーを表示
echo do_shortcode( get_field( 'gallery' ) );
// $str = do_shortcode( get_field( 'gallery' ) );
// echo preg_replace( '/(<a.+?)(><img)/', '$1 class="shutterset_set_2"$2', $str );
?>

<?php echo my_add_spec( $content ); // functions/itemlist.php にて定義 ?>

<?php endwhile; endif; ?>
</div>

</div>
<!-- コンテンツ領域終了 -->

<!-- お問い合わせ共通ブロック -->
<?php get_template_part( 'contact' ); ?>

</div><!-- #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
