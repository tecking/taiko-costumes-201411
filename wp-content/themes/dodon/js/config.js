/*
 * jQuery
 */
jQuery(document).ready(function($) {
	
	// ボックスの高さを揃える
	$('.home #itemlist li p').tile();
	$('.home #blogfeed dl').tile();
	
	// Colorbox
	$('a.group1').colorbox({
		title: true,
		rel: 'group1',
		maxWidth: '80%',
		maxHeight: '80%'
	});
	$('a.group2').colorbox({
		title: true,
		rel: 'group2',
		maxWidth: '80%',
		maxHeight: '80%'
	}); 
});
