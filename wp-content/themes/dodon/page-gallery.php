<?php get_header(); ?>

<div id="content" role="main">

<!-- パンくずリスト -->
<div id="bredcrumb" >
<a href="<?php echo get_option('home'); ?>">Home</a> &raquo; 
<?php foreach ( array_reverse(get_post_ancestors($post->ID)) as $parid ) { ?>
<a href="<?php echo get_page_link( $parid );?>" title="<?php echo get_page($parid)->post_title; ?>">
<?php echo get_page($parid)->post_title; ?></a> &raquo; 
<?php } ?>
<?php the_title(); ?>
</div>


<?php get_template_part( 'pagetitle' ); ?>

<!-- コンテンツ領域開始 -->
<?php
$post_name = $post -> post_name;
?>
<div id="post" class="<?php echo esc_attr( $post_name ); ?>">

<div class="entry-content">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<?php get_template_part('gallery-search'); ?>
	
	<?php the_content(); ?>

<?php endwhile;endif; ?>
</div>

</div>
<!-- コンテンツ領域終了 -->


<!-- お問い合わせ共通ブロック -->
<?php
$page = get_page_by_title( '衣装について' );
if ( $post -> post_parent == $page -> ID ) get_template_part( 'content', 'costume_footer' );
?>

<?php get_template_part( 'contact' ); ?>


</div><!-- #content -->


<?php get_sidebar(); ?>
<?php get_footer(); ?>
