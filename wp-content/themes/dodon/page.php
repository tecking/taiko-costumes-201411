<?php get_header(); ?>

<div id="content" role="main">

<!-- パンくずリスト -->
<div id="bredcrumb" >
<a href="<?php echo get_option('home'); ?>">Home</a> &raquo; 
<?php foreach ( array_reverse(get_post_ancestors($post->ID)) as $parid ) { ?>
<a href="<?php echo get_page_link( $parid );?>" title="<?php echo get_page($parid)->post_title; ?>">
<?php echo get_page($parid)->post_title; ?></a> &raquo; 
<?php } ?>
<?php the_title(); ?>
</div>


<?php get_template_part( 'pagetitle' ); ?>

<!-- コンテンツ領域開始 -->
<?php
$post_name = $post -> post_name;
?>
<div id="post" class="<?php echo esc_attr( $post_name ); ?>">

<div class="entry-content">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<!-- 衣装一覧リストページの時に一覧メニューを表示 -->
		<?php if(is_page('134')): ?>

			<?php wp_nav_menu( array('menu' => 'itemlist' )); ?>

		<!-- 衣装についてリストページの時に一覧メニューを表示 -->
		<?php elseif(is_page('166')): ?>

			<?php wp_nav_menu( array('menu' => 'about-costume' )); ?>

		<!-- ご注文ガイドリストページの時に一覧メニューを表示 -->
		<?php elseif(is_page('183')): ?>

			<?php wp_nav_menu( array('menu' => 'guidemenu' )); ?>


		<!-- 太鼓リンク集ページの時はこちらを表示 -->
		<?php elseif(is_page('474')): ?>

			<ul>
			<?php wp_list_bookmarks('title_li=&categorize=0&category=15&between=<br />&show_description=1'); ?>
			</ul>

<?php else : ?>
<!-- それ以外は通常表示 -->

	
	<?php the_content(); ?>


<?php endif; ?>


<?php endwhile;endif; ?>
</div>

</div>
<!-- コンテンツ領域終了 -->


<!-- お問い合わせ共通ブロック -->
<?php
$page = get_page_by_title( '衣装について' );
if ( $post -> post_parent == $page -> ID ) get_template_part( 'content', 'costume_footer' );
?>

<?php get_template_part( 'contact' ); ?>


</div><!-- #content -->


<?php get_sidebar(); ?>
<?php get_footer(); ?>
