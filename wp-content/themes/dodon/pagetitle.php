<!-- ページタイトル -->
<?php
if ( is_page() ) {
	$url = $_SERVER['REQUEST_URI'];
	if ( preg_match ( "/gallery/", $url ) ) {
		$filename = 'gallery';
		$alt      = '衣装アルバム';
	}
	elseif ( preg_match ( "/contact/", $url ) ) {
		$filename = 'contact';
		$alt      = 'お見積り・お問い合わせフォーム';
	}
	else {
		$filename = $post -> post_name;
		$alt      = $post -> post_title;
	}
	$filename .= '.jpg';
}
else {
	$filename = '';
	$alt      = '';
}
if ( is_archive() && get_query_var( 'post_type' ) === 'voice' ) $filename = 'voices.jpg';
if ( is_search() ) $filename = 'gallery.jpg';
?>
<div id="pagetitle"
><h2><img src="<?php echo get_bloginfo ( 'stylesheet_directory' ) . '/img/contents/pagetitle_' . $filename . '" alt="' . $alt; ?>"
/></h2></div>
<!-- ページタイトル -->
