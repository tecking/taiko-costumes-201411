<?php get_header(); ?>

<div id="content" role="main">

<!-- パンくずリスト -->
<div id="bredcrumb">
<a href="<?php echo home_url(); ?>">Home</a> » <a href="<?php echo home_url('gallery'); ?>">衣装アルバム</a> » 「<?php echo esc_attr( get_search_query() ); ?>」の検索結果</div>

<?php get_template_part( 'pagetitle' ); ?>

<!-- コンテンツ領域開始 -->
<div id="post">

	<div class="entry-content gallery">

		<?php get_template_part('gallery-search'); ?>

		<!--<?php do_shortcode( '[nggallery]' ); ?>-->
		<?php if ( function_exists('ngg_images_results') ) ngg_images_results(); ?>

	</div>

</div>
<!-- コンテンツ領域終了 -->

<!-- お問い合わせ共通ブロック -->
<?php get_template_part( 'contact' ); ?>


</div><!-- #content -->


<?php get_sidebar(); ?>
<?php get_footer(); ?>
