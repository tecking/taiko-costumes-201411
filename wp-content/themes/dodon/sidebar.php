<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

		<div id="primary" class="widget-area" role="complementary">
			<ul class="mainbanner">
			<li><a href="<?php echo home_url( 'contactform' ); ?>" ><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/common/submenu_bnr_contact.jpg" alt="お見積り・お問い合わせフォーム" />太鼓衣装についてのお問い合わせはこちらから</a></li>
			<li class="contact"><a href="<?php echo home_url( 'sampleform' ); ?>" ><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/common/submenu_bnr_catalog.jpg" alt="衣装カタログ・生地サンプル無料送付いたします" />衣装カタログ・生地サンプル送付します。</a></li>
			<li class="contact"><a href="<?php echo home_url( 'sizesample' ); ?>" ><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/common/submenu_bnr_size.jpg" alt="sizesample" />衣装サイズのサンプル送付します。</a></li>
			<li><a href="<?php echo home_url( 'archives/category/showcase' ); ?>" ><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/common/submenu_bnr_work.jpg" alt="制作事例" />お作りした衣装の紹介します。</a></li>
			<li><a href="<?php echo home_url( 'gallery' ); ?>" ><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/common/submenu_bnr_photoalbum.jpg" alt="衣装アルバム" />デザインや柄の参考になります。</a></li>
			<li><a href="<?php echo get_post_type_archive_link( 'voice' ); ?>" ><img src="<?php echo bloginfo('stylesheet_directory'); ?>/img/common/submenu_bnr_yorokobi.jpg" alt="制作した太鼓チームからの喜びの声" />喜びの声は当店の励みです。</a></li>

			<?php

			
			ob_start();
			dynamic_sidebar( 'primary-widget-area' );
			$out = ob_get_clean();
			
			$patterns = array(
				'/(<div )(class="textwidget".+?facebook\.com)/s' );
			$replacements = array(
				'$1style="text-align: center;" $2' );
			echo preg_replace( $patterns, $replacements, $out );
			?>

			</ul>
		</div><!-- #primary .widget-area -->

<?php
	// A second sidebar for widgets, just because.
	if ( is_active_sidebar( 'secondary-widget-area' ) ) : ?>

		<div id="secondary" class="widget-area" role="complementary">
			<ul class="xoxo">
				<?php dynamic_sidebar( 'secondary-widget-area' ); ?>
			</ul>
		</div><!-- #secondary .widget-area -->

<?php endif; ?>
