<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

			<div id="content" role="main">

			<!-- パンくずリスト -->
			<div id="bredcrumb" >
			<a href="<?php echo get_option('home'); ?>">Home</a> &raquo; 
			
			<?php
			// if ( get_query_var( 'post_type' ) !== 'voices' ) {
			// $cat = get_the_category();
			// echo get_category_parents($cat[0], true, ' &raquo; ');
			// }
			?>  
			<?php the_title(); ?>
			</div>
			

			<?php
			/* Run the loop to output the post.
			 * If you want to overload this in a child theme then include a file
			 * called loop-single.php and that will be used instead.
			 */
			get_template_part( 'loop', 'single' );
			?>

			<?php get_template_part( 'contact' ); ?>
			</div><!-- #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
